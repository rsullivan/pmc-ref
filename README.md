# PMC reference checker #
##https://pmc-ref.herokuapp.com/##
###A prototype###

This is the implementation deployed to Heroku. The original code is here https://bitbucket.org/rsullivan/cc-browser.

Given a digital object identifier (doi) PMC reference checker will return the percentage of an article's references that are free to access in [PubMed Central](http://www.ncbi.nlm.nih.gov/pmc/). It can also return the details of the references that are free to acess. An API is also available at https://pmc-ref.herokuapp.com/api. PMCrc is still very much in alpha.

### What is this repository for? ###

* For researchers who would like freely accessible research.
